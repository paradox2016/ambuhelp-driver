### install ionic
>npm install -g cordova ionic

### build project
>npm install

>bower install

### web preview
> ionic serve --labs


### build for android
>ionic hooks add

>ionic platform add android`

>ionic state restore --plugins

>ionic resources

>cordova prepare

>ionic build android

**generated apk path**: `platforms/android/build/outputs/apk/android-debug.apk
`

## Test Credentials 

>**username**: KA51-AA-0001 to KA51-AA-0010

>**password**: driver

## Latest APK file 

[app link for wipro](https://wipro365-my.sharepoint.com/personal/sa342798_wipro_com/_layouts/15/guestaccess.aspx?guestaccesstoken=gWs%2f3no%2bhM58G%2bH9AqUvdiPsquVmM0O156RsTcQFOAQ%3d&docid=2_128a28848fb274aa18bf84812495ea968&rev=1)

[app link for outside](https://drive.google.com/file/d/0B-_1rYo4xecIWnZiWUd6WHlmQlk/view?usp=sharing)