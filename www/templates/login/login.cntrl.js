app.controller('loginCtrl', function ($scope, $rootScope, $state, $stateParams, $ionicLoading, $ionicPlatform, AmbulanceModel, LoopBackAuth, LoopBackResource, socket) {
    
    if (LoopBackAuth.currentUserId !== null) {
        $rootScope.show("Already logged in. Redirecting!");
        AmbulanceModel.findOne({
            filter: {
                where: {
                    id: LoopBackAuth.currentUserId
                },
                include: ["driverDetails"]
            }
        }).$promise.then(function (ambulanceDetail) {
            $rootScope.ambulanceDetail = ambulanceDetail;
          
            $rootScope.hide();
            $state.go('home');
        }, function (err) {
            $rootScope.hide();
            $rootScope.showAlert(err.statusText, err.data.error.message, err);
        });
    }


    $scope.credentials = {}

    $scope.doLogin = function () {
        $rootScope.show('Logging in!');
        $scope.loginResult = AmbulanceModel.login({
                rememberMe: true
            }, $scope.credentials)
            .$promise.then(function (res) {
                AmbulanceModel.findOne({
                    filter: {
                        where: {
                            id: LoopBackAuth.currentUserId
                        },
                        include: ["driverDetails"]
                    }
                }).$promise.then(function (ambulanceDetail) {
                    $rootScope.ambulanceDetail = ambulanceDetail;
                   
                    $rootScope.hide();
                    $state.go('home');
                }, function (err) {
                    $rootScope.hide();
                    $rootScope.showAlert(err.statusText, err.data.error.message, err);
                });

            }, function (err) {
                $rootScope.hide();
                $rootScope.showAlert(err.statusText, err.data.error.message, err);
            });

    };



});