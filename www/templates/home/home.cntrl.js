app.controller('homeCntrl', function ($scope, $rootScope, $state, $interval, $timeout, $stateParams, LoopBackAuth, MapUtility,
    $ionicModal, $ionicPopup, $ionicPlatform, $ionicPopover, $cordovaNativeAudio, $cordovaGeolocation, $cordovaLaunchNavigator, AmbulanceModel, socket, PubSub) {


    $scope.status = {};
    $scope.status.busy = false;

    var locationSynced = false;

    $scope.sos = {};
    $scope.sos.isPicked = false;

    $scope.currentDestination;
    $scope.currentPosition;

    $scope.emergencyData = {};

    var mapDiv = document.getElementById("map");

    var locationUpdateInterval;

    $scope.popoverTemplates = {
        menu: 'templates/home/menu.popover.html',
        emergencyData: 'templates/home/emergencyType.popover.html',
        callContacts: 'templates/home/contactList.popover.html'
    }

    var mapIcon = {
        myIcon: "img/map-marker.png",
        sosIcon: "img/sos_marker.png",
        hospitalIcon: "img/hospital_map_icon.png"
    }

    var options = {
        timeout: 20000,
        enableHighAccuracy: true
    };

    var watchOptions = {
        timeout: 3000,
        enableHighAccuracy: false // may cause errors if true
    };

    var pathProperties = new google.maps.Polyline({
        strokeColor: '#000000',
        strokeOpacity: 0.8,
        strokeWeight: 5
    });

    var directionsService = new google.maps.DirectionsService();
    var directionsRenderer = new google.maps.DirectionsRenderer({
        polylineOptions: pathProperties
    });
    directionsRenderer.setOptions({
        suppressMarkers: true
    });

    var getDirectionRequest = function (origin, destination) {
        return {
            origin: origin,
            destination: destination,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        }
    }

    var renderDirections = function () {

        directionsService.route(getDirectionRequest($scope.currentPosition, $scope.currentDestination), function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsRenderer.setDirections(response);
            }
        });

    }

    var reCalibrateBound = function () {

        var bounds = new google.maps.LatLngBounds();
        bounds.extend($scope.currentPosition);
        bounds.extend($scope.currentDestination);

        $scope.map.setCenter(bounds.getCenter());
        $scope.map.fitBounds(bounds);

        //remove one zoom level to ensure no marker is on the edge.
        $scope.map.setZoom($scope.map.getZoom() - 1);

        // set a minimum zoom 
        // if you got only 1 marker or all markers are on the same address map will be zoomed too much.
        if ($scope.map.getZoom() > 15) {
            $scope.map.setZoom(15);
        }

    }

    var addHospitalMarker = function (position) {
        var positionMarker = new google.maps.LatLng(position.lat, position.lng);
        $scope.currentDestination = positionMarker;
        $scope.hospitalMarker = MapUtility.putMarker($scope.map, $scope.currentDestination,
            MapUtility.getMapIcon(mapIcon.hospitalIcon, 36));
        reCalibrateBound();
        renderDirections();

    }

    var addSOSMarker = function (position) {
        var positionMarker = new google.maps.LatLng(position.lat, position.lng);
        $scope.currentDestination = positionMarker;
        $scope.sosMarker = MapUtility.putMarker($scope.map, $scope.currentDestination,
            MapUtility.getMapIcon(mapIcon.sosIcon, 36));
        reCalibrateBound();
        renderDirections();
    }


    $scope.$on('$ionicView.enter', function () {
        // Code you want executed every time view is opened

        if ($scope.allowNextLogin) {
            console.log("Re Login");
            $scope.allowNextLogin = false;
            socket.connect();
        }
    });

    /* Popover Functions*/

    $scope.openPopover = function (themeUrl, $event) {
        $ionicPopover.fromTemplateUrl(themeUrl, {
            scope: $scope

        }).then(function (popover) {
            $scope.popover = popover;
            $scope.popover.show($event);
        });

    }

    $scope.closePopover = function (cb) {
        $scope.popover.hide().then(function () {
            $scope.popover.remove();
            cb();
        });

    }

    /* Specific Popovers*/

    $scope.showMenu = function ($event) {
        $scope.openPopover($scope.popoverTemplates.menu, $event);
    }

    $scope.openProfile = function () {
        $scope.closePopover(function () {});
        $state.go("profile");
    }

    /* Logout Functionality*/

    $scope.doLogout = function () {
        $scope.closePopover(function () {});

        $scope.allowNextLogin = true;

        $rootScope.show('Logging Out... Please Wait');
        socket.disconnect();
        AmbulanceModel.logout(function () {
            $rootScope.hide();
            $state.go('login');
        });

    }

    /* Modal Functions*/

    $scope.openModal = function (themeUrl, $event) {
        $ionicModal.fromTemplateUrl(themeUrl, {
            scope: $scope

        }).then(function (modal) {
            $scope.modal = modal;
            $scope.modal.show($event);
        });

    }

    $scope.closeModal = function (cb) {
        $scope.modal.hide().then(function () {
            $scope.modal.remove();
            cb();
        });

    };


    /* SOS Request Utils */

    $scope.pickSOSRequest = function ($event) {
        // Take Health Status.

        $scope.openModal($scope.popoverTemplates.emergencyData, $event);

    }

    $scope.completeSOSRequest = function () {
        $scope.hospitalMarker.setMap(null);
        var data = {
            id: $scope.bookingDetail.id,
            type: 'request-complete',
            hospitalId: $scope.bookingDetail.hospital.id,
            userId: $scope.bookingDetail.user.id,
            ambulanceId: $scope.bookingDetail.ambulance.id
        }
        PubSub.publish({
            collectionName: 'SOSRequestUpdate',
            method: 'POST',
            data: data
        });

        // Reset Location
        console.log(locationUpdateInterval);
        $interval.cancel(locationUpdateInterval);
        locationUpdateInterval = undefined;

        directionsRenderer.setMap(null);
        $scope.sayThankyou();
        $scope.moveToCenter();
    }

    $scope.sayThankyou = function () {
        var alertPopup = $ionicPopup.alert({
            title: 'Thanks a lot!',
            template: '<div class="text-center"><h1 class="icon ion-ios-heart assertive"></h1><h4>Thank you for saving a life. We appreciate your effort</h4></div>'
        });

        alertPopup.then(function (res) {
            $scope.sos.isPicked = false;
            $scope.status.busy = false;
        });
    };


    $scope.closeEmegencyDataPopover = function () {
        $scope.closeModal(function () {
            $scope.sos.isPicked = true;
            $scope.sosMarker.setMap(null);
            addHospitalMarker($scope.bookingDetail.hospital.hospitalDetails.mapLocation);

            var data = {
                id: $scope.bookingDetail.id,
                type: 'pickup-update',
                hospitalId: $scope.bookingDetail.hospital.id,
                userId: $scope.bookingDetail.user.id,
                ambulanceId: $scope.bookingDetail.ambulance.id,
                data: $scope.emergencyData
            }
            PubSub.publish({
                collectionName: 'SOSRequestUpdate',
                method: 'POST',
                data: data
            });

            $scope.emergencyData = {
                emergencyType: '',
                emergencyState: ''
            };
        });
    };


    $scope.openCallContactPopover = function ($event) {
        console.log('getting called');
        $scope.openModal($scope.popoverTemplates.callContacts, $event);
    };
    $scope.closeCallContactPopover = function () {
        $scope.closeModal(function () {});
    };



    $scope.emergencyTypeList = ['Heart Attack', 'Paralysis Attack', 'Accident', 'Poisoning'];
    $scope.emergencyStateList = ['Critical', 'High', 'Normal', 'Low'];

    /* Map Code */

    var initMap = function () {
        var bangaloreCenter = new google.maps.LatLng(12.9538477, 77.3507442);
        if(angular.isUndefined($scope.currentPosition)){
            $scope.currentPosition = angular.copy(bangaloreCenter);
        }
        
        var mapOptions = MapUtility.getDefaultMapOption(bangaloreCenter);
        $scope.map = new google.maps.Map(mapDiv, mapOptions);
    }



    $scope.moveToCenter = function () {
        $scope.map.panTo($scope.currentPosition)
    }


    $scope.doNavigate = function (pos) {

        $cordovaLaunchNavigator.navigate([pos.lat(), pos.lng()], {
            enableDebug: true
        }).then(function () {
            alert("Navigator launched");
        }, function (err) {
            alert(err);
        });
    }



    var initiateSOSTrip = function () {

        if (!$scope.status.busy) {
            $cordovaNativeAudio.play('notification');
            $scope.status.busy = true;
            addSOSMarker($scope.bookingDetail.user.location);
            directionsRenderer.setMap($scope.map);
        }


    }


    var putCurrentPositionOnMap = function () {

        $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
            $scope.currentPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var mapOptions = MapUtility.getDefaultMapOption($scope.currentPosition);

        }, function (error) {
            console.log("Could not get location");
        });



        $scope.watch = $cordovaGeolocation.watchPosition(watchOptions).then(null,
            function (err) {
                console.log(err);
            },
            function (position) {

                $scope.currentPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            });

    }


    $ionicPlatform.ready(function () {
        $cordovaNativeAudio.preloadSimple('notification', 'audio/notification.mp3');
        initMap();
        putCurrentPositionOnMap();

    });

    function syncLocationWithSocket() {
        if ($scope.currentPosition!==null && !angular.isUndefined($scope.currentPosition)) {
            /* EMIT current location*/
            console.log('socket sync called');
            var location = {
                lat: $scope.currentPosition.lat(),
                lng: $scope.currentPosition.lng()
            }
            socket.emit('update-location', {
                id: LoopBackAuth.currentUserId,
                userType: 'driver',
                location: location
            });
            if ($scope.myMarker!==null && !angular.isUndefined($scope.myMarker)) {
                $scope.myMarker.setPosition($scope.currentPosition);
            } else {
                $scope.myMarker = MapUtility.putMarker($scope.map, $scope.currentPosition, MapUtility.getMapIcon(mapIcon.myIcon, 25));
                $scope.map.panTo($scope.myMarker.getPosition());
            }
            if ($scope.status.busy) {
                renderDirections();
            }
        } else {
            console.log('waiting for current position');
        }
    }

    $rootScope.$watch('isSocketConnected', function () {
        if ($rootScope.isSocketConnected) {
            syncLocationWithSocket();
        } else {
            console.log('waiting for socket connect');
        }
    });

    $scope.$watch('currentPosition', function () {
        syncLocationWithSocket();
        
        locationSynced = true;

    });

    PubSub.subscribe({
        collectionName: 'SOSRequest',
        modelId: LoopBackAuth.currentUserId,
        method: 'POST'
    }, function (bookingRequest) {
        $scope.bookingDetail = bookingRequest;
        initiateSOSTrip();
        $scope.$apply();

        locationUpdateInterval = $interval(function () {

            if (!$scope.status.busy) {
                $interval.cancel(locationUpdateInterval);
            } else {
                var data = {
                    id: $scope.bookingDetail.id,
                    location: $scope.currentPosition
                }
                console.log("updating location");
                PubSub.publish({
                    collectionName: 'LocationUpdate',
                    method: 'POST',
                    data: data
                });
            }


        }, 3000);
    })

});