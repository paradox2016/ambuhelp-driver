angular.module('app.services', [])

.service('$cordovaLaunchNavigator', ['$q', function ($q) {
    var $cordovaLaunchNavigator = {};

    $cordovaLaunchNavigator.navigate = function (destination, options) {
        var q = $q.defer(),
            isRealDevice = ionic.Platform.isWebView();
            console.log(launchnavigator)
        if (!isRealDevice) {
            q.reject("launchnavigator will only work on a real mobile device! It is a NATIVE app launcher.");
        } else {
            try {

                var successFn = options.successCallBack || function () {},
                    errorFn = options.errorCallback || function () {},
                    _successFn = function () {
                        successFn();
                        q.resolve();
                    },
                    _errorFn = function (err) {
                        errorFn(err);
                        q.reject(err);
                    };

                options.successCallBack = _successFn;
                options.errorCallback = _errorFn;


                launchnavigator.isAppAvailable(launchnavigator.APP.GOOGLE_MAPS, function (isAvailable) {
                    var app;
                    console.log(launchnavigator.APP);
                    if (isAvailable) {
                        app = launchnavigator.APP.GOOGLE_MAPS;
                    } else {
                        alert("Google Maps not available - falling back to user selection");
                        app = launchnavigator.APP.USER_SELECT;
                    }
                    options.app = app
                    options.launchMode = launchnavigator.LAUNCH_MODE.TURN_BY_TURN;
                    launchnavigator.navigate(destination, options);
                });


            } catch (e) {
                q.reject("Exception: " + e.message);
            }
        }
        return q.promise;
    };



    return $cordovaLaunchNavigator;
  }]);
