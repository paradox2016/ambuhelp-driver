angular.module('map.services', [])
    .factory('MapUtility', function ($http) {
        return {
            getDefaultMapOption: function (myLatlng) {
                return {
                    center: myLatlng,
                    zoom: 12,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    disableDefaultUI: true,
                    zoomControl: false
                };
            },
            getMapIcon: function (markerUrl, size) {
                return {
                    url: markerUrl,
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (24, 48).
                    anchor: new google.maps.Point((size / 2), size)
                }
            },
            setMapCircle: function (map, pos, radius) {
                return new google.maps.Circle({
                    strokeColor: '#c0392b',
                    strokeOpacity: 0.60,
                    strokeWeight: 2,
                    fillColor: '#e74c3c',
                    fillOpacity: 0.20,
                    map: map,
                    center: pos,
                    radius: radius * 1000
                });
            },
            putMarker: function (map, pos, icon) {
                return new google.maps.Marker({
                    map: map,
                    position: pos,
                    icon: icon
                });
            },
            addInfoWindow: function (map, marker, message) {
                var infoWindow = new google.maps.InfoWindow({
                    content: message
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infoWindow.open(map, marker);
                });
            },
            findPathsFromOriginToDestination: function (origin, destinationCoords, cb) {
                var distanceMatrixService = new google.maps.DistanceMatrixService();
                distanceMatrixService.getDistanceMatrix({
                    origins: [origin],
                    destinations: destinationCoords,
                    travelMode: 'DRIVING',
                    unitSystem: google.maps.UnitSystem.METRIC,
                    avoidHighways: false,
                    avoidTolls: false
                }, function (response, status) {
                    if (status !== 'OK') {
                        cb('error in distance Matrix', null);
                    } else {
                        cb(null, response.rows[0].elements);
                    }

                });

            },
            encodeCoordinates: function (pos) {
                return polyline.encode([[pos.lat(), pos.lng()]]);
            },
            decodeCoordinate: function (encodedPoints) {
                return polyline.decode(encodedPoints);
            }

        }

    })
    .factory('RotateIconUtil', function () {
            var RotateIcon = function (options) {
                this.options = options || {};
                this.rImg = options.img || new Image();
                this.rImg.src = this.rImg.src || this.options.url || '';
                this.options.width = this.options.width || this.options.size || this.rImg.width;
                this.options.height = this.options.height || this.options.size || this.rImg.height;
                var canvas = document.createElement("canvas");
                canvas.width = this.options.width;
                canvas.height = this.options.height;
                this.context = canvas.getContext("2d");
                this.canvas = canvas;
            };

            RotateIcon.prototype.setRotation = function (options) {
                var canvas = this.context,
                    angle = options.deg ? options.deg * Math.PI / 180 :
                    options.rad,
                    centerX = this.options.width / 2,
                    centerY = this.options.height / 2;

                canvas.clearRect(0, 0, canvas.width, canvas.height);
                canvas.save();
                
                canvas.translate(centerX, centerY);
                canvas.rotate(angle);
                canvas.translate(-centerX, -centerY);
                canvas.drawImage(this.rImg, 0, 0,options.size,options.size);
                canvas.restore();
                return this;
            };
            RotateIcon.prototype.getUrl = function () {
                return this.canvas.toDataURL('image/png');
            };

            RotateIcon.makeIcon = function (image,size) {
                return new RotateIcon({
                        img: image,
                        size:size
                    });
            };

            return {
                putRotateMarkerIcon: function (map, position, img, size, rotation) {
                    var halfSize = size/2;
                    var rotateMarker = new google.maps.Marker({
                        position: position,
                        map: map,
                        icon: {
                            url: RotateIcon.makeIcon(img,size)
                                .setRotation({ deg: rotation,size: size})
                                .getUrl(),
                            origin: new google.maps.Point(0,0)
                        }

                    });
                    return rotateMarker;

                }
            }
        });