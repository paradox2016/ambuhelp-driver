angular.module('app.routes', [])

.config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login/login.tpl.html',
            controller: 'loginCtrl'
        })

        .state('home', {
            url: '/home',
            templateUrl: 'templates/home/home.tpl.html',
            controller: 'homeCntrl'
        })

        .state('history', {
            url: '/history',
            templateUrl: 'templates/history/history.tpl.html',
            controller: 'historyCtrl'
        })

        .state('profile', {
            url: '/profile',
            templateUrl: 'templates/profile/profile.tpl.html',
            controller: 'profileCtrl'
        })

    $urlRouterProvider.otherwise('/login')



});